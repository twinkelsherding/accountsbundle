<?php

namespace TWH\AccountsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class TWHAccountsBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
